import 'package:flutter/material.dart';

Container _buildTextTitle(String title) {
  return Container(
    padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border: Border.all(width: 3, color: Color.fromRGBO(153, 204, 255, 1))),
    margin: const EdgeInsets.all(10),
    child: Text(title,
        style: TextStyle(fontSize: 20), textAlign: TextAlign.center),
  );
}

Column _buildIconSkill(String imgName, String name) {
  return Column(
    children: [
      Image.asset(
        imgName,
        width: 48,
      ),
      Container(
        child: Text(name),
      )
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget sectionContact = Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border:
                Border.all(width: 3, color: Color.fromRGBO(153, 204, 255, 1))),
        margin: const EdgeInsets.only(bottom: 20),
        child: Text('Contact',
            style: TextStyle(fontSize: 20), textAlign: TextAlign.center),
      ),
      Row(
        children: [
          Icon(Icons.call),
          Container(
            padding: const EdgeInsets.only(left: 10),
            margin: const EdgeInsets.only(bottom: 10),
            child: Text('061-4610568'),
          ),
        ],
      ),
      Row(
        children: [
          Icon(Icons.mail),
          Container(
            padding: const EdgeInsets.only(left: 10),
            margin: const EdgeInsets.only(bottom: 10),
            child: Text('61160042@go.buu.ac.th'),
          ),
        ],
      ),
    ],
  );

  Widget sectionAboutMe = Container(
    padding: const EdgeInsets.all(10),
    child: Text(
      'My name is Rujirada Norasarn and my nickname is cherry. My birthday is 27 January 1999. I am twenty two years old.',
      softWrap: true,
      style: TextStyle(fontSize: 17),
    ),
  );

  Widget sectionEducation = Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              '2012-2018',
              style: TextStyle(fontSize: 15),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                child: Text(
                  'Science-Math Program',
                  style: TextStyle(fontSize: 15),
                ),
              ),
              Container(
                child: Text(
                  'Phothisamphanphitthayakhan School',
                  style: TextStyle(color: Colors.grey),
                ),
              )
            ],
          )
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              '2018-now',
              style: TextStyle(fontSize: 15),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                child: Text(
                  'Bachelor of Computer Science',
                  style: TextStyle(fontSize: 15),
                ),
              ),
              Container(
                child: Text(
                  'Burapha University',
                  style: TextStyle(color: Colors.grey),
                ),
              )
            ],
          )
        ],
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: const Text(
              'RUJIRADA NORASARN',
              style: TextStyle(color: Colors.black),
            ),
            backgroundColor: Color.fromRGBO(153, 204, 255, 1),
          ),
          body: Container(
            margin: const EdgeInsets.all(10),
            child: ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      'images/me.jpg',
                      width: 150,
                    ),
                    sectionContact
                  ],
                ),
                _buildTextTitle('About Me'),
                sectionAboutMe,
                _buildTextTitle('Skills'),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    _buildIconSkill('images/java.png', 'Java'),
                    _buildIconSkill('images/html.png', 'HTML'),
                    _buildIconSkill('images/css.png', 'CSS'),
                    _buildIconSkill('images/js.jpg', 'JS'),
                    _buildIconSkill('images/node.png', 'Node.js'),
                    _buildIconSkill('images/py.jpg', 'Python'),
                    _buildIconSkill('images/vue.png', 'Vue'),
                  ],
                ),
                _buildTextTitle('Education'),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: sectionEducation,
                ),
                Container(
                  height: 20,
                  color: Color.fromRGBO(153, 204, 255, 1),
                )
              ],
            ),
          )),
    );
  }
}
